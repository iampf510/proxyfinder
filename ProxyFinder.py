#!/usr/bin/env python3

from requests_html import HTMLSession 
import random
import time

def Cybersyndrome():
    global proxy
    url = 'http://www.cybersyndrome.net/search.cgi?q=&a=ABCD&f=s&s=new&n=500&p='
    s = HTMLSession()
    r = s.get(url+'1')
    total = int(r.html.find('.pagelinks a')[-2].text)
    for i in range(1,total+1):
        u = f'{url}{i}'
        print(f'{u}')
        r = s.get(u)
        r.html.render()
        num = len(r.html.find('#div_result table tr')[1:])
        print(f'num : {num}')
        for n in range(num):
            ip, port = r.html.find('#n{n+1}').text.split(':')
            print(f'{ip}:{port}')
            if ip not in proxy:
                proxy[ip] = {'port':port, 'type':'https'}
       
def CoolProxy():
    global proxy
    url = 'https://www.cool-proxy.net/proxies/http_proxy_list/sort:score/direction:desc/page:'
    s = HTMLSession()
    r = s.get(url+'1')
    total = int(r.html.find('.pagination span')[-2].text)
    for i in range(1, total+1):
        u = f'{url}{i}'
        print(f'{u}')
        r = s.get(u)
        r.html.render()
        num = r.html.find('table tr')[1:]
        for n in num:
            try:
                ip, port = n.text.split(')))')[1].split('\n')[:2]
                if ip not in proxy:
                    proxy[ip] = {'port':port, 'type':'https'}
            except:
                pass

def PremProxy():
    global proxy
    url = 'https://premproxy.com/list/'
    s = HTMLSession()
    r = s.get(url+'01.htm')
    total = int(r.html.find('.pagination')[-1].text.split('\n')[-2])
    for i in range(1, total+1):
        u = f'{url}{i:02d}.htm'
        print(u)
        r = s.get(u)
        r.html.render()
        for p in r.html.find('.anon'):
            ip, port = p.text.split(' ')[1].split('\n')[0].split(':')
            if ip not in proxy:
                proxy[ip] = {'port':port, 'type':'https'}

class ProxyFetcher:
    def __init__(self):
        self.ua = open('ua-list', 'r').readlines() 

    def start(self):
        #Cybersyndrome()
        #CoolProxy()
        PremProxy()

    def pickUA(self):
        return random.choice(self.ua)

    def save(self):
        f = open('proxy_list', 'w')
        global proxy
        f.write('ip,port,type\n')
        for ip in proxy:
            f.write(f'{ip},{proxy[ip]["port"]},{proxy[ip]["type"]}\n')
        f.close()

if __name__ == '__main__':
    proxy = {}

    p = ProxyFetcher()

    p.start()
    p.save()
